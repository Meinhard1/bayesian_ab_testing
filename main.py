# Created by meinhardmare at 2021-07-30
"""
Do a basic A/B test to  get familiar with the pymc3 library
"""
import pymc as pm
import scipy.stats as stats
import numpy as np
import arviz as az
import matplotlib.pyplot as plt

# generate some random data
blue_conversions = stats.bernoulli.rvs(0.1, size=15000)  # conversion of 10% and 1500 obs
red_conversions = stats.bernoulli.rvs(0.11, size=7000)  # conversion of 11% and 375 obs

# What is the mean
blue_conversions.mean() # 0.1002
red_conversions.mean() # 0.1097

# Set up the pymc model. Assume Uniform priors for p_A and p_B as these are not truly known
with pm.Model():
    # define the prior
    blue_rate = pm.Uniform('blue_rate', 0, 1)
    red_rate = pm.Uniform('red_rate', 0, 1)

    # Define the deterministic delta function. This is our unknown of interest.
    delta = pm.Deterministic("delta", blue_rate - red_rate)

    # Set of observations, in this case we have two observation datasets.
    blue_obs = pm.Bernoulli('blue_obs', blue_rate, observed=blue_conversions)
    red_obs = pm.Bernoulli('red_obs', red_rate, observed=red_conversions)

    # get the samples
    # trace = pm.sample(return_inferencedata=True)
    trace = pm.sample(chains=4, cores=1, return_inferencedata=True)

# reconstruct the posterior distributions of the conversion rates
az.plot_posterior(trace)
plt.show()

# Count the number of samples less than 0, i.e. the area under the curve
# before 0, represent the probability that blue_conversions is worse than red_conversions.
delta_samples = trace.posterior['delta'].values
print(f"Probability blue_conversions is WORSE than red_conversions: {np.mean(delta_samples < 0):.2%}")
print(f"Probability blue_conversions is BETTER than red_conversions: {np.mean(delta_samples > 0):.2%}")

# Probability blue_conversions is WORSE than red_conversions: 95.12%
# Probability blue_conversions is BETTER than red_conversions: 4.88%