# bayesian A/B testing

Can we use bayesian statistics to do A/B testing? Yes, we can!
An alternative to the classical frequentist approach is to use bayesian statistics.

The idea came for reading this bolg post:
* https://towardsdatascience.com/bayesian-a-b-testing-in-pymc3-54dceb87af74

These are useful links:
* https://nbviewer.jupyter.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter2_MorePyMC/Ch2_MorePyMC_PyMC3.ipynb
* https://camdavidsonpilon.github.io/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/

